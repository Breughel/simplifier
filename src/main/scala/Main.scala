
import java.io.FileReader
import java.io.FileNotFoundException
import java.io.IOException

import AST.{NodeList, Node}
import Simplifier.Simplifier

object Main {

  def main(args: Array[String]) {
//      println(parseString("x+y=c"))
      if(args.length == 0) {
        println("Usage: sbt \"run filename ...\"")
        return
      }

      val parser = new Parser()
      println(new java.io.File(".").getCanonicalPath)
      for (arg <- args) {
          try {
              println("Parsing file: " + arg)
              val reader = new FileReader(arg)
              val parseResult = parser.parseAll(reader)
              parseResult match {
                 case parser.Success(result: List[AST.Node], in) => {
                     println("\nAST:")
                     println(parseResult)
                     val tree = AST.NodeList(result)
                     val simplifiedTree = Simplifier.simplify(tree)
                     println("\nAST after optimization:")
                     println(simplifiedTree)
                     println("\nProgram after optimization:")
                     println(simplifiedTree.toStr)
                 }
                 case parser.NoSuccess(msg: String, in) => println("FAILURE " + parseResult)
              }
          }
          catch {
              case ex: FileNotFoundException => println("Couldn't open file " + arg)
              case ex: IOException => println("Couldn't read file " + arg)
          }
      }
  }

def parseString(str: String): Node = {
  val parser = new Parser()
  val parseResult = parser.parseAll(parser.program, str+"\n")

  parseResult match {
    case parser.Success(result: List[AST.Node], in) => Simplifier.simplify(NodeList(result))
    case parser.NoSuccess(msg: String, in) => throw new IllegalArgumentException("FAILURE Could not parse '" + str + "': " + msg)
  }
}
}

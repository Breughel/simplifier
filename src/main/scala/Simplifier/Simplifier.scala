package Simplifier

import AST._

import scala.collection.immutable.HashSet

// to implement
// avoid one huge match of cases
// take into account non-greedy strategies to resolve cases with power laws
object Simplifier {
  def simplify(node: Node): Node = {
    node match {
      case node: NodeList => nodeList(node)
      case BinExpr(op, left, right)  => binExpr(BinExpr(op, simplify(left), simplify(right)))
      case Unary(opt, expr) => unary(Unary(opt, simplify(expr)))
      case IfElseInstr(cond, left, right) => ifElseInstr(IfElseInstr(simplify(cond), simplify(left), simplify(right)))
      case IfInstr(cond, body) => ifInstr(IfInstr(simplify(cond), simplify(body)))
      case Assignment(left, right) => assignment(Assignment(left, simplify(right)))
      case WhileInstr(cond, body) => whileInstr(WhileInstr(simplify(cond), simplify(body)))
      case IfElseExpr(cond, left, right) => ifElseExpr(IfElseExpr(simplify(cond), simplify(left), simplify(right)))
      case ElemList(list) => ElemList(list.map(a => simplify(a)))
      case KeyDatumList(list) => keyDatumList(KeyDatumList(list.map(a => a match {case KeyDatum(k, v) => KeyDatum(simplify(k), simplify(v))})))
      case Subscription(expr, sub) => Subscription(simplify(expr), simplify(sub))
      case KeyDatum(key, value) => KeyDatum(simplify(key), simplify(value))
      case GetAttr(expr, attr) => GetAttr(simplify(expr), attr)
      case ReturnInstr(expr) => ReturnInstr(simplify(expr))
      case PrintInstr(expr) => PrintInstr(simplify(expr))
      case FunCall(name, args) => FunCall(simplify(name), simplify(args))
      case FunDef(name, args, body) => FunDef(name, simplify(args), simplify(body))
      case LambdaDef(args, body) => LambdaDef(simplify(args), simplify(body))
      case ClassDef(name, list, suite) => ClassDef(name, simplify(list), simplify(suite))
      case ElemList(list) => ElemList(list.map(a => simplify(a)))
      case Tuple(list) => Tuple(list.map(a => simplify(a)))
      case _ => node
    }
  }

  def nodeList(node: NodeList): Node = {
    def removeDeadAssignments(list: List[Node]): List[Node] = {
      list match {
        case (a1: Assignment) :: (a2: Assignment) :: t =>
          if (a1.left != a2.left) a1 :: removeDeadAssignments(a2 :: t)
          else a2.right match {
            case Variable(x) if x == a2.left.indent => removeDeadAssignments(a1 :: t)
            case e: Variable => removeDeadAssignments(a2 :: t)
            case e: IntNum => removeDeadAssignments(a2 :: t)
            case e: FloatNum => removeDeadAssignments(a2 :: t)
            case _ => a1 :: removeDeadAssignments(a2 :: t)
          }
        case a1 :: a2 :: t => a1 :: removeDeadAssignments(a2 :: t)
        case _ => list
      }
    }
    val result = removeDeadAssignments(node.list.map(a => simplify(a)))
    if (result.nonEmpty) result.head else NodeList(result)
  }

  def keyDatumList(node: KeyDatumList): Node = {
    def getUnique(set: HashSet[Node], list: List[KeyDatum]): List[KeyDatum] = {
      list match {
        case h :: t if set.contains(h.key) => getUnique(set, t)
        case h :: t => h :: getUnique(set + h.key, t)
        case nil => nil
      }
    }
    KeyDatumList(getUnique(HashSet[Node](), node.list.reverse))
  }

  def ifElseExpr(node: IfElseExpr): Node = {
    node match {
      case IfElseExpr(TrueConst(), instr, _) => instr
      case IfElseExpr(FalseConst(), _, instr) => instr
      case _ => node
    }
  }

  def assignment(node: Assignment): Node = {
    node match {
      case Assignment(Variable(x), Variable(y)) if x == y => NodeList(List())
      case _ => node
    }
  }

  def whileInstr(node: WhileInstr): Node = {
    node match {
      case WhileInstr(FalseConst(), _) => NodeList(List())
      case WhileInstr(TrueConst(), instr) => instr
      case _ => node
    }
  }

  def ifElseInstr(node: IfElseInstr): Node = {
    node match {
      case IfElseInstr(TrueConst(), instr, _) => instr
      case IfElseInstr(FalseConst(), _, instr) => instr
      case _ => node
    }
  }

  def ifInstr(expr: IfInstr): Node = {
    expr match {
      case IfInstr(TrueConst(), instr) => instr
      case IfInstr(FalseConst(), _) => NodeList(List())
      case _ => expr
    }
  }

  def unary(node: Unary): Node = {
    node match {
      case Unary("not", e: TrueConst) => FalseConst()
      case Unary("not", e: FalseConst) => TrueConst()
      case Unary("not", BinExpr(op, left, right)) => op match {
        case "!=" => BinExpr("==", left, right)
        case "==" => BinExpr("!=", left, right)
        case "<=" => BinExpr(">", left, right)
        case ">=" => BinExpr("<", left, right)
        case ">" => BinExpr("<=", left, right)
        case "<" => BinExpr(">=", left, right)
      }
      case Unary(opt1, Unary(opt2, expr)) if opt1 == opt2 => expr
      case _ => node
    }
  }

  def binExpr(node: BinExpr): Node = node match {
    case BinExpr("+", left: ElemList, right: ElemList) => ElemList(List.concat(left.list, right.list))
    case BinExpr("+", left: Tuple, right: Tuple) => Tuple(List.concat(left.list, right.list))

    case BinExpr(op @ ("+" | "-" | "*" | "**" | "/" | "%"), left: IntNum, right: IntNum) => IntNum(calculate(op, left.toDouble, right.toDouble).toInt)
    case BinExpr(op @ ("+" | "-" | "*" | "**" | "/" | "%"), left: IntNum, right: FloatNum) => FloatNum(calculate(op, left.toDouble, right.toDouble))
    case BinExpr(op @ ("+" | "-" | "*" | "**" | "/" | "%"), left: FloatNum, right: FloatNum) => FloatNum(calculate(op, left.toDouble, right.toDouble))
    case BinExpr(op @ ("+" | "-" | "*" | "**" | "/" | "%"), left: FloatNum, right: IntNum) => FloatNum(calculate(op, left.toDouble, right.toDouble))

    case BinExpr("*", BinExpr("**", left1, right1), BinExpr("**", left2, right2)) if left1 equals left2 => simplify(BinExpr("**", left1, BinExpr("+", right1, right2)))

    case BinExpr("or", left, right) if left equals right => left
    case BinExpr("or", _, TrueConst()) => TrueConst()
    case BinExpr("or", TrueConst(), _) => TrueConst()
    case BinExpr("or", left, FalseConst()) => left

    case BinExpr("or", FalseConst(), right) => right
    case BinExpr("and", TrueConst(), right) => right
    case BinExpr("and", left, TrueConst()) => left
    case BinExpr("and", _, FalseConst()) => FalseConst()
    case BinExpr("and", FalseConst(), _) => FalseConst()
    case BinExpr("and", left, right) if left equals right => left

    case BinExpr("==", left, right) if left equals right => TrueConst()
    case BinExpr(">=", left, right) if left equals right => TrueConst()
    case BinExpr("<=", left, right) if left equals right => TrueConst()
    case BinExpr("<", left, right) if left equals right => FalseConst()
    case BinExpr(">", left, right) if left equals right => FalseConst()
    case BinExpr("!=", left, right) if left equals right => FalseConst()

    case BinExpr("*", left, FloatNum(x)) if x == 1 => left
    case BinExpr("*", FloatNum(x), right) if x == 1 => right
    case BinExpr("*", left, FloatNum(x)) if x == 0 => IntNum(0)
    case BinExpr("*", FloatNum(x), right) if x == 0 => IntNum(0)
    case BinExpr("*", left, IntNum(x)) if x == 1 => left
    case BinExpr("*", IntNum(x), right) if x == 1 => right
    case BinExpr("*", left, IntNum(x)) if x == 0 => IntNum(0)
    case BinExpr("*", IntNum(x), right) if x == 0 => IntNum(0)

    case BinExpr("+", left, FloatNum(x)) if x == 0 => left
    case BinExpr("+", FloatNum(x), right) if x == 0 => right
    case BinExpr("+", left, IntNum(x)) if x == 0 => left
    case BinExpr("+", IntNum(x), right) if x == 0 => right

    case BinExpr("/", left, IntNum(x)) if x == 1 => left
    case BinExpr("/", left, FloatNum(x)) if x == 1 => left
    case BinExpr("/", x, y) if x equals y => IntNum(1)
    case BinExpr("/", x, BinExpr("/", y, z)) if x equals y => z
    case BinExpr("*", x, BinExpr("/", IntNum(y), z)) if y == 1 => BinExpr("/", x, z)
    case BinExpr("*", x, BinExpr("/", FloatNum(y), z)) if y == 1 => BinExpr("/", x, z)

    case BinExpr("**", _, IntNum(x)) if x == 0 => IntNum(1)
    case BinExpr("**", _, FloatNum(x)) if x == 0 => FloatNum(1)
    case BinExpr("**", left, IntNum(x)) if x == 1 => left
    case BinExpr("**", left, FloatNum(x)) if x == 1 => left

    case BinExpr("-", x, y) if x equals y => IntNum(0)
    case BinExpr("+", Unary("-", x), y) if x equals y => IntNum(0)
    case BinExpr("-", BinExpr(op @ ("+" | "-"), left, right), variable @ Variable(name)) => simplify(BinExpr(op, BinExpr("-", left, variable), right))

    case BinExpr("**", BinExpr("**", x, n), m) => simplify(BinExpr("**", x, BinExpr("*", n, m)))
    case BinExpr("+", BinExpr("+", BinExpr("**", a1, IntNum(sc1)), BinExpr("*", BinExpr("*", IntNum(sc2), a2), b1)), BinExpr("**", b2, IntNum(sc3))) if a1 == a2 && b1 == b2 && sc1 == sc2 && sc2 == sc3 && sc3 == 2 => simplify(BinExpr("**", BinExpr("+", a1, b1),IntNum(2)))
    case _ => node
  }

  def calculate(op: String, x: Double, y: Double): Double = op match {
    case "+" => x+y
    case "-" => x-y
    case "*" => x*y
    case "/" => x/y
    case "%" => x%y
    case "**" => math.pow(x, y)
  }
}
